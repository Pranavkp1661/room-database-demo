package com.example.roomdatabasedemo;

import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.ViewHolder> {

    private List<MainData> dataList;
    private final Activity context;
    private RoomDB database;

    public RvAdapter(Activity context, List<MainData> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public void updateAdapter(List<MainData> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RvAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RvAdapter.ViewHolder holder, int position) {
        MainData mainData = dataList.get(position);
        database = RoomDB.getInstance(context);

        holder.imvEdit.setOnClickListener(view -> {
            MainData mainData1 = dataList.get(holder.getAdapterPosition());
            int sID = mainData1.getId();
            String sText = mainData1.getText();
            Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_update_layout);
            int width = WindowManager.LayoutParams.MATCH_PARENT;
            int height = WindowManager.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.show();

            EditText etUpdate = dialog.findViewById(R.id.etUpdate);
            Button btUpdate = dialog.findViewById(R.id.btUpdate);

            etUpdate.setText(sText);

            btUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    String uText = etUpdate.getText().toString().trim();
                    database.mainDao().update(sID, uText);
                    dataList.clear();
                    dataList.addAll(database.mainDao().getAll());
                    notifyDataSetChanged();
                }
            });
        });
        holder.imvDelete.setOnClickListener(view -> {
            MainData mainData12 = dataList.get(holder.getAdapterPosition());
            database.mainDao().delete(mainData12);
            int position1 = holder.getAdapterPosition();
            dataList.remove(position1);
            notifyItemRemoved(position1);
            notifyItemRangeChanged(position1, dataList.size());
        });
        holder.tvItemText.setText(dataList.get(position).getText());

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemText;
        ImageButton imvEdit;
        ImageButton imvDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemText = itemView.findViewById(R.id.tvItemText);
            imvEdit = itemView.findViewById(R.id.imvEdit);
            imvDelete = itemView.findViewById(R.id.imvDelete);
        }
    }
}
