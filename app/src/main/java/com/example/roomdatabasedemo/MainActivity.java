package com.example.roomdatabasedemo;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    EditText etText;
    Button btAdd;
    Button btReset;
    RecyclerView rvItem;
    List<MainData> dataList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    RoomDB database;
    RvAdapter rvAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        etText = findViewById(R.id.etText);
        btAdd = findViewById(R.id.btAdd);
        btReset = findViewById(R.id.btReset);
        rvItem = findViewById(R.id.rvItem);

        database = RoomDB.getInstance(this);
        dataList = database.mainDao().getAll();
        linearLayoutManager = new LinearLayoutManager(context);
        rvItem.setLayoutManager(linearLayoutManager);
        rvAdapter = new RvAdapter(MainActivity.this, dataList);
        rvItem.setAdapter(rvAdapter);

        btAdd.setOnClickListener(view -> {
            String sText = etText.getText().toString().trim();

            if (!sText.equals("")) {
                MainData mainData = new MainData();
                mainData.setText(sText);
                database.mainDao().insert(mainData);
                etText.setText("");
                dataList.clear();
                dataList.addAll(database.mainDao().getAll());
                rvAdapter.updateAdapter(dataList);
            }
        });
        btReset.setOnClickListener(view -> {
            database.mainDao().reset(dataList);
            dataList.clear();
            dataList.addAll(database.mainDao().getAll());
            rvAdapter.updateAdapter(dataList);
        });
    }
}